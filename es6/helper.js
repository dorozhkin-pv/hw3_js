const parentNode = window.document.querySelector('tbody');

function clearParentNode () {   //Ф-ция очистки parentNode
    parentNode.innerHTML = '';
}

const state = (() => getCookies())();   //Начальное состояние!!!

function getCookies() {     //Получаю удобный массив объектов с куками
    if (window.document.cookie != '') {
        return window.document.cookie.split(';')
        .map(item => item.split('='))
        .map(item => ({name: item[0], value: item[1]}));
    } else {
        return [];
    }
}

function createElement(cookies) {  //Передаю массив, получаю верстку

    cookies.forEach((element, i) => {
        const tr = window.document.createElement('TR');

        const th = window.document.createElement('TH');
        th.setAttribute('scope', 'row');
        th.innerHTML = i + 1;

        const td_1 = window.document.createElement('TD');
        td_1.dataset.name = '';
        td_1.innerHTML = element.name;

        const td_2 = window.document.createElement('TD');
        td_2.dataset.value = '';
        td_2.innerHTML = element.value;

        const td_3 = window.document.createElement('TD');
        td_3.innerHTML = element.maxAge || '-- --';

        const td_4 = window.document.createElement('TD');
        const btn = window.document.createElement('BUTTON');
        btn.setAttribute('type', 'button');
        btn.className = 'btn btn-danger';
        btn.innerHTML = 'X';
        td_4.append(btn);

        tr.append(th);
        tr.append(td_1);
        tr.append(td_2);
        tr.append(td_3);
        tr.append(td_4);

        parentNode.append(tr);
    });
}

function showElement () {                           //Скрывает/показывает форму
    const btn = window.document.querySelector('#showForm');
    const myForm = window.document.forms[0];
    
    btn.addEventListener('click', () => {
        myForm.classList.toggle('active');
    });
}
showElement();

export {parentNode, clearParentNode, state, createElement}