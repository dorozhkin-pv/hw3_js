import {clearParentNode, state, createElement} from './helper.js'

const addForm = window.document.querySelector('.add-form');

function addCookie(e) {

    e.preventDefault();

    const obj = {
        name: addForm.input1.value,
        value: addForm.input2.value,
        maxAge: addForm.input3.value
    };

    const maxAge = obj.maxAge * 24 * 3600;  //Срок жизни max-age

    const isEmpty = Object.values(obj).some( item => item == '');   //Проверяю заполнены ли поля

    if (!isEmpty && maxAge > 0) {
        window.document.cookie = `${obj.name}=${obj.value}; max-age=${maxAge}`;	//добавляю куку

        clearParentNode();  //Очищаю данные
        state.splice(state.length, 0, {name: obj.name, value: obj.value, maxAge: obj.maxAge});  //Обновляю state
        createElement(state);     //Обновляю данные на странице

        addForm.reset();    //Очищаю форму
    } else {
        window.alert('Заполните все поля формы!');
    }
}

export {addForm, addCookie}