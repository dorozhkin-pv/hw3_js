import {clearParentNode, state, createElement} from './helper.js'

function detectNode(e) {
    const target = e.target;

    if(target.tagName == 'BUTTON') {
        
        const name = target.parentNode.parentNode.querySelector('[data-name]').innerHTML;
        const value = target.parentNode.parentNode.querySelector('[data-value]').innerHTML;
        const id = target.parentNode.parentNode.querySelector('[scope]').innerHTML - 1;

        const data = {
            name,
            value
        }

        if (deleteCookie(data)) {
            clearParentNode();
            state.splice(id, 1);
            createElement(state);
        }
    }
}

function deleteCookie(data) {
    const res = window.confirm(`Вы действительно хотите удалить ${data.name}`);
    
    if (res) {
        window.document.cookie = `${data.name}=${data.value}; max-age=0`;
    }

    return res;
}

export {detectNode, deleteCookie}