import {parentNode, state, createElement} from './helper.js'
import {detectNode} from './deleteCookie.js'
import {addForm, addCookie} from './addCookie.js'

window.addEventListener('load', function(){
    
    createElement(state);
    parentNode.addEventListener('click', detectNode);
    addForm.addEventListener('submit', addCookie);
    
});